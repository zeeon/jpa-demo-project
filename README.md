# Дополнительные ссылки
1. https://docs.spring.io/spring-data/jpa/docs/current/reference/html/#jpa.query-methods
2. Дополнительные примеры для работа с BoostedJDBC https://gitlab.tcsbank.ru/invest-auto-qa/ixxi-db-tests
3. JDBCTemplate https://www.baeldung.com/spring-jdbc-jdbctemplate
4. Create Entitu Jpa Buddy, POJO generator https://plugins.jetbrains.com/plugin/12297-pojo-generator https://plugins.jetbrains.com/plugin/15075-jpa-buddy
