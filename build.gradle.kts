group = "ru.tinkoff.jpa.demo"
version = "1.0-SNAPSHOT"

plugins {
    java
    application
    id("org.springframework.boot") version "2.4.5"
    id("io.freefair.lombok") version "6.0.0-m2"
}


repositories {
    maven {
        url = uri("https://nexus-new.tcsbank.ru/content/repositories/inv-platform-core/")
    }
    maven {
        url = uri("https://nexus-new.tcsbank.ru/content/repositories/mvn-maven-proxy/")
    }
    maven {
        url = uri("https://nexus-new.tcsbank.ru/content/repositories/mvn-invest-sdet-releases/")
    }
    maven {
        url = uri("https://nexus-new.tcsbank.ru/content/repositories/mvn-confluent-proxy/")
    }
}

configure<JavaPluginConvention> {
    sourceCompatibility = JavaVersion.VERSION_11
    targetCompatibility = JavaVersion.VERSION_11
}

dependencies {
    implementation("org.jetbrains:annotations:19.0.0")
    val springVersion = "2.4.5"

    testImplementation("org.springframework.boot:spring-boot-starter-test:$springVersion") {
        exclude("junit", "junit")
        exclude("org.junit.vintage", "junit-vintage-engine")
    }
    implementation("ru.tinkoff.inv.platform.core.postgres:postgres-enum:1.0.0")
    implementation("org.springframework.boot:spring-boot-starter-data-jpa:$springVersion")
    implementation("org.springframework.boot:spring-boot-starter-json:$springVersion")

    implementation("com.vladmihalcea:hibernate-types-52:2.12.1")

    testImplementation("org.junit.jupiter:junit-jupiter:5.7.2")

    implementation("org.assertj:assertj-core:3.19.0")

    // Postgres
    implementation("org.postgresql:postgresql:42.2.24")
    implementation("ru.tinkoff.invest.sdet:boosted-database-operations:0.0.5")

    // Allure
    implementation("io.qameta.allure:allure-java-commons:2.13.5")
    implementation("io.qameta.allure:allure-junit5:2.13.5")

    // Async waiters
    implementation("org.awaitility:awaitility:4.1.0")
}

tasks.bootJar {
    enabled = false
}

tasks.test {
    useJUnitPlatform()
}