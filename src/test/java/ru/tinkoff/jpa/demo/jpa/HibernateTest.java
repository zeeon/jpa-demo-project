package ru.tinkoff.jpa.demo.jpa;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.jdbc.Sql;
import ru.tinkoff.jpa.demo.jpa.hibernate.configuration.SocialDbConfig;
import ru.tinkoff.jpa.demo.jpa.hibernate.entities.Contract;
import ru.tinkoff.jpa.demo.jpa.hibernate.entities.Instrument;
import ru.tinkoff.jpa.demo.jpa.hibernate.entities.Profile;
import ru.tinkoff.jpa.demo.jpa.hibernate.entities.enums.Currency;
import ru.tinkoff.jpa.demo.jpa.hibernate.entities.enums.InstrumentType;
import ru.tinkoff.jpa.demo.jpa.hibernate.services.ContractService;
import ru.tinkoff.jpa.demo.jpa.hibernate.services.InstrumentService;
import ru.tinkoff.jpa.demo.jpa.hibernate.services.ProfileService;
import ru.tinkoff.jpa.demo.jpa.hibernate.someBusinessSteps.BusinessStep;

import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@Slf4j
@SpringBootTest(classes = {SocialDbConfig.class})
@Sql({"/test-instrument-data.sql"})
public class HibernateTest {
    UUID id = UUID.fromString("eca71448-7699-45ac-81a3-a423b041b2ab");

    @Autowired
    ContractService contractService;
    @Autowired
    ProfileService profileService;
    @Autowired
    InstrumentService instrumentService;
    @Autowired
    BusinessStep businessStep;

    @Test
    void hibernateSelectTest() {

        Profile profileById = profileService.getProfileById(id);

        businessStep.newStep("2019698428", profileById.getId());

        Assertions.assertEquals(id, profileById.getId());
    }

    @Test
    void hibernateSelectQueryTest() {

        profileService.getRandomProfile();

    }

    @Test
    void hibernateUpdateTest() {
        Profile profileById = profileService.getProfileById(id);
        String siebelId = profileById.getSiebelId();
        profileById.setSiebelId("12345");

        profileService.updateProfile(profileById);

        Assertions.assertEquals("12345", profileById.getSiebelId());

        profileById.setSiebelId(siebelId);
        profileService.updateProfile(profileById);
    }

    @Test
    void hibernateInsertAndDeleteTest() {
        Instrument instrument = new Instrument();
        instrument.setTicker("12345")
            .setClassCode("12345")
            .setBriefName("12345")
            .setPriceTs(LocalDateTime.now())
            .setLastPrice(20.20)
            .setOtcFlag(true)
            .setYield(10.10)
            .setYieldTs(LocalDateTime.now())
            .setRelativeYield(15.15)
            .setRelativeYieldTs(LocalDateTime.now())
            .setMinPriceIncrement(2.20)
            .setType(InstrumentType.CURRENCY)
            .setCurrency(Currency.RUB);

        Instrument instrumentInserted = instrumentService.updateInstrument(instrument);

        Assertions.assertEquals(instrument.getTicker(), instrumentInserted.getTicker());
        Assertions.assertEquals(instrument.getClassCode(), instrumentInserted.getClassCode());

        instrumentService.deleteInstrument(instrumentInserted.getId());
    }

    @Test
    void hibernateDeleteTest() {
        List<Instrument> listInstrumentByTicker = instrumentService.getListInstrumentByTicker("12345");
        listInstrumentByTicker.forEach(instrument -> instrumentService.deleteInstrument(instrument.getId()));
    }
}
