package ru.tinkoff.jpa.demo.jpa;


import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import ru.tinkoff.jpa.demo.jpa.hibernate.configuration.SocialDbConfig;
import ru.tinkoff.jpa.demo.jpa.hibernate.entities.Profile;
import ru.tinkoff.jpa.demo.jpa.hibernate.services.JdbcProfileService;

import java.util.UUID;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@Slf4j
@SpringBootTest(classes = {SocialDbConfig.class})
public class BoostedJdbcTest {
    UUID id = UUID.fromString("eca71448-7699-45ac-81a3-a423b041b2ab");

    @Autowired
    JdbcProfileService jdbcProfileService;

    @Test
    void boostedJdbcTest() {
        UUID profileById = jdbcProfileService.getProfileById(id);
        log.info("Profile data: {}", profileById);

        Profile profile = jdbcProfileService.getProfilebyTemplate(id);
        log.info("Profile data: {}", profile);
    }
}
