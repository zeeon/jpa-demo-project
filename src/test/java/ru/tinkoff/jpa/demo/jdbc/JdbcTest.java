package ru.tinkoff.jpa.demo.jdbc;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import ru.tinkoff.jpa.demo.jdbc.controller.DbController;
import ru.tinkoff.jpa.demo.jdbc.entities.Profile;

import java.util.UUID;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@Slf4j
public class JdbcTest {
    UUID id = UUID.fromString("eca71448-7699-45ac-81a3-a423b041b2ab");

    @Test
    void jdbcSelectTest() {
        Profile profileById = DbController.getProfileById(id);
        log.info("Profile Data: {}", profileById);
        Assertions.assertEquals(id, profileById.getId());
    }

    @Test
    void jdbcUpdateTest() {
        Profile profileById = DbController.getProfileById(id);
        DbController.changeSiebelId("12345", id);
        log.info("Profile Data: {}", profileById);
        Profile profileAfterUpdate = DbController.getProfileById(id);
        log.info("Profile Data after update: {}", profileAfterUpdate);
        Assertions.assertEquals("12345", profileAfterUpdate.getSiebelId());

        DbController.changeSiebelId(profileById.getSiebelId(), id);

    }
}
