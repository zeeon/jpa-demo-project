package ru.tinkoff.jpa.demo.jdbc.connection;

import lombok.extern.slf4j.Slf4j;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

@Slf4j
public class DbConnectionFactory {
    public static Connection getConnection() throws SQLException {
        String url = "jdbc:postgresql://pg-invest-social-qa.ds.invest.cloud:5432/social";
        String login = "social";
        String password = "social";
        Connection connection = DriverManager.getConnection(url, login, password);
        try (PreparedStatement pst = connection.prepareStatement("SELECT 1")) {
            pst.executeQuery();
            log.info("Connection was successfully opened.");
        } catch (SQLException ex) {
            log.info("There are errors while the connection opening.");
            ex.printStackTrace();
        }
        return connection;
    }
}
