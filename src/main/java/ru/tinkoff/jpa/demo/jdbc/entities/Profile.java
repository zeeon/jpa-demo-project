package ru.tinkoff.jpa.demo.jdbc.entities;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.UUID;

@Data
@Accessors(chain = true)
@JsonIgnoreProperties(ignoreUnknown = true)
public class Profile {
    @JsonProperty("id")
    private UUID id;
    private Long position;
    private String siebelId;
    private String nickname;
}
