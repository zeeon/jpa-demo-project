package ru.tinkoff.jpa.demo.jdbc.controller;

import lombok.experimental.UtilityClass;
import lombok.extern.slf4j.Slf4j;
import ru.tinkoff.jpa.demo.jdbc.connection.DbConnectionFactory;
import ru.tinkoff.jpa.demo.jdbc.entities.Profile;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.UUID;

@UtilityClass
@Slf4j
public class DbController {

    public static Profile getProfileById(UUID id) {
        Profile profile = new Profile();
        try (Connection connection = DbConnectionFactory.getConnection();
             PreparedStatement pst = connection.prepareStatement("SELECT * " +
                 "FROM profile " +
                 "WHERE id = uuid(?)")) {
            pst.setString(1, id.toString());
            try (ResultSet result = pst.executeQuery()) {
                if (result.next()) {
                    profile.setId(result.getObject("id", java.util.UUID.class))
                        .setPosition(result.getLong("position"))
                        .setSiebelId(result.getString("siebel_id"))
                        .setNickname(result.getString("nickname"));
                }
            }
        } catch (SQLException ex) {
            log.info("Error. Profile data for id = '{}' wasn't received. Error message {}", id, ex.getMessage());
            throw new AssertionError(ex);
        }
        log.info("Profile data for id = '{}' was received", id);
        return profile;
    }

    public static boolean changeSiebelId(String siebelId, UUID id) {
            try (Connection connection = DbConnectionFactory.getConnection();
                 PreparedStatement pst = connection.prepareStatement("UPDATE profile " +
                     "SET siebel_id = ?\n" +
                     "WHERE id = uuid(?)")) {
                pst.setString(1, siebelId);
                pst.setString(2, id.toString());
                pst.executeUpdate();
                log.info("Profile with id = '{}' changed the siebelId to '{}'", id, siebelId);
                return true;
            } catch (SQLException ex) {
                log.info("Error while the changing of the receipt status. Error message {}", ex.getMessage());
                throw new AssertionError(ex);
            }
        }

    }