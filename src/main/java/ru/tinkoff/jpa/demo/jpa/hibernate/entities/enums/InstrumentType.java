package ru.tinkoff.jpa.demo.jpa.hibernate.entities.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum InstrumentType {
    BOND("bond"),
    CURRENCY("currency"),
    ETF("etf"),
    SHARE("share"),
    SP("sp"),
    FUTURES("futures");

    @Getter
    private final String value;
}
