package ru.tinkoff.jpa.demo.jpa.hibernate.entities.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum Currency {
    RUB("rub"),
    USD("usd"),
    EUR("eur"),
    CAD("cad"),
    CHF("chf"),
    GBP("gbp"),
    HKD("hkd"),
    NOK("nok"),
    SEK("sek"),
    TRY("try"),
    TRL("trl"),
    ILS("ils"),
    CNY("cny"),
    JPY("jpy");

    @Getter
    private final String value;
}
