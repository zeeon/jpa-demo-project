package ru.tinkoff.jpa.demo.jpa.hibernate.entities;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonValue;
import com.vladmihalcea.hibernate.type.array.ListArrayType;
import com.vladmihalcea.hibernate.type.json.JsonType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;
import ru.tinkoff.inv.platform.core.postgres.PostgresEnumCommon;
import ru.tinkoff.jpa.demo.jpa.hibernate.entities.enums.ProfileStatus;
import ru.tinkoff.jpa.demo.jpa.hibernate.entities.enums.ProfileType;


import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

@Entity
@Builder(toBuilder = true)
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "profile")
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@TypeDefs({
    @TypeDef(
        name = "pgsql_enum",
        typeClass = PostgresEnumCommon.class),
    @TypeDef(
        name = "list-array",
        typeClass = ListArrayType.class),
    @TypeDef(
        name = "json",
        typeClass = JsonType.class)
})
public class Profile {
    @Id
    @Column(name = "id")
    @JsonProperty("id")
    private UUID id;

    @Column(name = "position")
    @JsonProperty("position")
    private Long position;

    @Column(name = "siebel_id")
    @JsonProperty("siebel_id")
    private String siebelId;

    @Column(name = "nickname")
    @JsonProperty("nickname")
    private String nickname;

    @Enumerated(EnumType.STRING)
    @Column(name = "status")
    @Type(type = "pgsql_enum")
    private ProfileStatus status;

    @Column(name = "image")
    private UUID image;

    @Column(name = "has_contract")
    private Boolean hasContract;

    @Column(name = "deleted")
    private Boolean deleted;

    @Column(name = "block")
    private Boolean block;

    @Column(name = "description")
    private String description;

    @Type(type = "list-array")
    @Column(name = "dictionary_ids", columnDefinition = "integer[]")
    private List<Integer> dictionaryIds;

    @Type(type = "list-array")
    @Column(name = "service_tags", columnDefinition = "varchar[]")
    private List<String> serviceTags;

    @Type(type = "list-array")
    @Column(name = "interested_tags", columnDefinition = "varchar[]")
    private List<String> interestedTags;

    @Type(type = "list-array")
    @Column(name = "interesting_tags", columnDefinition = "varchar[]")
    private List<String> interestingTags;

    @Column(name = "followers_count")
    private Long followersCount;

    @Column(name = "following_count")
    private Long followingCount;

    @Column(name = "validated")
    private Boolean validated;

    @Column(name = "interested_bitmap")
    private Long interestedBitmap;

    @Column(name = "interesting_bitmap")
    private Long interestingBitmap;

    @Column(name = "inserted")
    private LocalDateTime inserted;

    @Column(name = "updated")
    private LocalDateTime updated;

    @Column(name = "registered_at")
    private LocalDateTime registeredAt;

    @Type(type = "json")
    @Column(name = "settings", columnDefinition = "jsonb")
    private String settings;

    @Column(name = "advisable_position")
    private Long advisablePosition;

    @Column(name = "hashtag_following_count")
    private Long hashtagFollowingCount;

    @Column(name = "muted_profiles_count")
    private Long mutedProfilesCount;

    @Type(type = "json")
    @Column(name = "popular_hashtags", columnDefinition = "jsonb")
    private String popularHashtags;

    @Column(name = "donation_active")
    private Boolean donationActive;

    @Type(type = "list-array")
    @Column(name = "internal_service_tags", columnDefinition = "varchar[]")
    private List<String> internalServiceTags;

    @Column(name = "donation_block")
    private Boolean donationBlock;

    @Enumerated(EnumType.STRING)
    @Column(name = "type")
    @Type(type = "pgsql_enum")
    private ProfileType type;

    @Column(name = "invest_id")
    private UUID investId;
}
