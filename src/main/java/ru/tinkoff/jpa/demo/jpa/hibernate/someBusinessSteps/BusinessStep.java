package ru.tinkoff.jpa.demo.jpa.hibernate.someBusinessSteps;

import io.qameta.allure.Step;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.tinkoff.jpa.demo.jpa.hibernate.services.ContractService;
import ru.tinkoff.jpa.demo.jpa.hibernate.services.ProfileService;


import java.util.UUID;

@Service
@RequiredArgsConstructor
public class BusinessStep {

    private final ContractService contractService;
    private final ProfileService profileService;

    @Step("Чтото делаем")
    public void newStep(String contractId, UUID profileId) {
        contractService.getContractById(contractId);
        profileService.getProfileById(profileId);
    }
}
