package ru.tinkoff.jpa.demo.jpa.hibernate.entities;

import lombok.Data;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;
import ru.tinkoff.inv.platform.core.postgres.PostgresEnumCommon;
import ru.tinkoff.jpa.demo.jpa.hibernate.entities.enums.ContractType;
import ru.tinkoff.jpa.demo.jpa.hibernate.entities.enums.TariffCategory;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.UUID;

@Entity
@Table(name = "contract")
@Data
@TypeDefs({
    @TypeDef(
        name = "pgsql_enum",
        typeClass = PostgresEnumCommon.class)
})
public class Contract {
    @Id
    @GeneratedValue
    @Column(name = "id")
    private String id;

    @Column(name = "profile_id")
    private UUID profileId;

    @Enumerated(EnumType.STRING)
    @Type(type = "pgsql_enum")
    @Column(name = "type")
    private ContractType type;

    @Column(name = "opened_at")
    private LocalDate openedAt;

    @Column(name = "year_relative_yield")
    private Double yearRelativeYield;

    @Column(name = "inserted")
    private LocalDateTime inserted;

    @Column(name = "updated")
    private LocalDateTime updated;

    @Enumerated(EnumType.STRING)
    @Type(type = "pgsql_enum")
    @Column(name = "tariff_category")
    private TariffCategory tariffCategory;
}
