package ru.tinkoff.jpa.demo.jpa.hibernate.services;

import io.qameta.allure.Step;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import ru.tinkoff.jpa.demo.jpa.hibernate.entities.Profile;
import ru.tinkoff.jpa.demo.jpa.hibernate.repositories.ProfileRepository;


import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static ru.tinkoff.jpa.demo.helper.AllureHelper.*;


@Slf4j
@Service
@RequiredArgsConstructor
public class ProfileService {
    private final ProfileRepository repository;

    @Step("Получить профиль по id-{id}")
    public Profile getProfileById(UUID id) {
        log.info("Получен запрос на получение profile по id: {}", id);
        Profile profile = repository.findFirstById(id);
        log.info("Найдены записи: {}", profile);
        addJsonAttachment(FOUND_ENTITIES, profile);
        return profile;
    }

    @Step("Получить рандомный профиль по status-open, hasContract-true, block-false")
    public Profile getRandomProfile() {
        log.info("Получен запрос на получение profile по status:{} hasContract:{} block:{}", "open", "true", "false");
        Optional<Profile> profile = repository.findByStatusAndContract();
        if (!profile.isPresent()) {
           throw new EntityNotFoundException("Профиль не найден");
        }
        log.info("Найдены записи: {}", profile);
        addJsonAttachment(FOUND_ENTITIES, profile);
        return profile.get();
    }

    @Step("Обновить профиль")
    public Profile updateProfile(Profile profile) {
        log.info("Получен запрос на обновление profile данные: {}", profile);
        repository.save(profile);
        log.info("Обновлена запись");
        addJsonAttachment(ADDED_ENTITIES, profile);
        return profile;
    }

    @Step("Удалить профиль по id")
    public void deleteProfile(UUID id) {
        log.info("Получен запрос на удаление profile по id: {}", id);
        List<Profile> profiles = repository.findAllById(id);
        repository.deleteAll(profiles);
        log.info("Удалена запись {}", profiles);
        addJsonAttachment(DELETED_ENTITIES, profiles);
    }
}
