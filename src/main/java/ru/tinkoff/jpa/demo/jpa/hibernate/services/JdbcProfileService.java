package ru.tinkoff.jpa.demo.jpa.hibernate.services;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.qameta.allure.Allure;
import io.qameta.allure.Step;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import ru.tinkoff.invest.sdet.boosted_database_operations.services.BoostedDatabaseOperations;
import ru.tinkoff.jpa.demo.jpa.hibernate.entities.Profile;
import ru.tinkoff.jpa.demo.jpa.hibernate.row_mapper.ProfileRowMapper;

import java.math.BigDecimal;
import java.util.Map;
import java.util.UUID;

@Slf4j
@Service
@RequiredArgsConstructor
public class JdbcProfileService {
    private final BoostedDatabaseOperations boostedDatabaseOperations;
    private final JdbcTemplate jdbcTemplate;
    private final ObjectMapper objectMapper;
    private final ProfileRowMapper profileRowMapper;


    @Step("Получение профиля по id")
    public UUID getProfileById(UUID id) {
        final var sql = "select id from profile where id = uuid(?)";
        log.info("Получение профиля по id {}", id);
        return boostedDatabaseOperations.query(sql, UUID.class, id.toString());
    }

    @Step("Получение профиля по id-{}")
    public Profile getProfilebyTemplate(UUID id) {
        final var sql = "select * from profile where id = uuid(?)";
        log.info("Получение профиля по id {}", id);
        return jdbcTemplate.queryForObject(sql, profileRowMapper, id.toString());
    }


}