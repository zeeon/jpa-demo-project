package ru.tinkoff.jpa.demo.jpa.hibernate.entities.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum ContractType {
    BROKER("broker"),
    PIA("pia");

    @Getter
    private final String value;
}
