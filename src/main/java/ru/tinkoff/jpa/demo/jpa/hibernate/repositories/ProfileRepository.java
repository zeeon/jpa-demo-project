package ru.tinkoff.jpa.demo.jpa.hibernate.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import ru.tinkoff.jpa.demo.jpa.hibernate.entities.Profile;


import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository
public interface ProfileRepository extends JpaRepository<Profile, UUID> {

    Profile findFirstById(UUID id);

    @Query(nativeQuery = true, value = "SELECT * FROM profile WHERE status = 'open' AND has_contract = true" +
        "  AND block = false AND type = 'personal' AND followers_count < 10" +
        "  AND NOT deleted AND random() < 0.01 LIMIT 1;")
    Optional<Profile> findByStatusAndContract();

    List<Profile> findAllById(UUID id);

}