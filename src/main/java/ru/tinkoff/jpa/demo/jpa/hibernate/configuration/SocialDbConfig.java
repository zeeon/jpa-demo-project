package ru.tinkoff.jpa.demo.jpa.hibernate.configuration;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.zaxxer.hikari.HikariDataSource;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.jackson.JacksonAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import ru.tinkoff.invest.sdet.boosted_database_operations.services.BoostedDatabaseOperations;

import javax.persistence.EntityManagerFactory;
import java.util.HashMap;

@ComponentScan("ru.tinkoff.jpa.demo.jpa.hibernate")
@Configuration
@Import({JacksonAutoConfiguration.class})
@EnableTransactionManagement
@EnableJpaRepositories(basePackages = "ru.tinkoff.jpa.demo.jpa.hibernate.repositories",
    entityManagerFactoryRef = "socialEntityManagerFactory",
    transactionManagerRef = "socialTransactionManager")
public class SocialDbConfig {

    @Bean
    @ConfigurationProperties("app.datasource.social")
    public DataSourceProperties socialDataSourceProperties() {
        return new DataSourceProperties();
    }

    @Bean(name = "socialJdbcTemplate")
    public JdbcTemplate socialJdbcTemplate() {
        return new JdbcTemplate(socialDataSource());
    }

    @Bean
    @ConfigurationProperties("app.datasource.social.configuration")
    public HikariDataSource socialDataSource() {
        return socialDataSourceProperties()
            .initializeDataSourceBuilder()
            .type(HikariDataSource.class)
            .build();
    }

    @Bean(name = "socialEntityManagerFactory")
    public LocalContainerEntityManagerFactoryBean socialEntityManagerFactory(
        @Qualifier("socialBuilder") EntityManagerFactoryBuilder builder) {
        return builder
            .dataSource(socialDataSource())
            .packages("ru.tinkoff.jpa.demo.jpa.hibernate.entities")
            .build();
    }

    @Bean(name = "socialTransactionManager")
    public PlatformTransactionManager socialTransactionManager(
        @Qualifier("socialEntityManagerFactory") EntityManagerFactory emf) {
        return new JpaTransactionManager(emf);
    }

    @Bean(name = "socialBuilder")
    public EntityManagerFactoryBuilder entityManagerFactoryBuilder() {
        return new EntityManagerFactoryBuilder(new HibernateJpaVendorAdapter(), new HashMap<>(), null);
    }
    @Bean
    @ConfigurationProperties("app.operations.social")
    BoostedDatabaseOperations fundsBoostedDatabaseOperations(
        @Qualifier("socialJdbcTemplate") JdbcTemplate boostedJdbcTemplate,
        ObjectMapper objectMapper) {
        return new BoostedDatabaseOperations(boostedJdbcTemplate, objectMapper);
    }
}
