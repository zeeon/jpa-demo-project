package ru.tinkoff.jpa.demo.jpa.hibernate.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.tinkoff.jpa.demo.jpa.hibernate.entities.Contract;
import ru.tinkoff.jpa.demo.jpa.hibernate.entities.Profile;
import ru.tinkoff.jpa.demo.jpa.hibernate.entities.enums.ContractType;


import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository
public interface ContractRepository extends JpaRepository<Contract, String> {

    Contract findFirstByProfileIdAndType(UUID profileId, ContractType type);

    List<Contract> findAllById(String id);
}
