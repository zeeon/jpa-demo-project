package ru.tinkoff.jpa.demo.jpa.hibernate.entities.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum TariffCategory {
    MASS_MARKET("mass-market"),
    WEALTH_MANAGEMENT("wealth-management"),
    PRIVATE("private"),
    UNAUTHORIZED("unauthorized");

    @Getter
    private final String value;
}
