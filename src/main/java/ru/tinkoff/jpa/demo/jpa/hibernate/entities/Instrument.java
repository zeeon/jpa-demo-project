package ru.tinkoff.jpa.demo.jpa.hibernate.entities;

import lombok.Data;
import lombok.experimental.Accessors;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;
import ru.tinkoff.inv.platform.core.postgres.PostgresEnumCommon;
import ru.tinkoff.jpa.demo.jpa.hibernate.entities.enums.Currency;
import ru.tinkoff.jpa.demo.jpa.hibernate.entities.enums.InstrumentType;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "instrument")
@Data
@Accessors(chain = true)
@TypeDefs({
    @TypeDef(
        name = "pgsql_enum",
        typeClass = PostgresEnumCommon.class)
})
public class Instrument {
    @Id
    @SequenceGenerator(name = "social.instrument_id_seq", sequenceName = "social.instrument_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "social.instrument_id_seq")
    @Column(name = "id")
    private Long id;

    @Column(name = "ticker")
    private String ticker;

    @Column(name = "class_code")
    private String classCode;

    @Enumerated(EnumType.STRING)
    @Type(type = "pgsql_enum")
    @Column(name = "type")
    private InstrumentType type;

    @Enumerated(EnumType.STRING)
    @Type(type = "pgsql_enum")
    @Column(name = "currency")
    private Currency currency;

    @Column(name = "image")
    private String image;

    @Column(name = "brief_name")
    private String briefName;

    @Column(name = "company")
    private String company;

    @Column(name = "sector")
    private String sector;

    @Column(name = "price_ts")
    private LocalDateTime priceTs;

    @Column(name = "last_price")
    private Double lastPrice;

    @Column(name = "otc_flag")
    private Boolean otcFlag;

    @Column(name = "yield_ts")
    private LocalDateTime yieldTs;

    @Column(name = "yield")
    private Double yield;

    @Column(name = "relative_yield_ts")
    private LocalDateTime relativeYieldTs;

    @Column(name = "relative_yield")
    private Double relativeYield;

    @Column(name = "min_price_increment")
    private Double minPriceIncrement;

    @Column(name = "amortization_flag")
    private Boolean amortizationFlag;

    @Column(name = "face_value")
    private Double faceValue;

    @Column(name = "absolute_price")
    private Double absolutePrice;

    @Column(name = "trading_clearing_account")
    private String tradingClearingAccount;
}
