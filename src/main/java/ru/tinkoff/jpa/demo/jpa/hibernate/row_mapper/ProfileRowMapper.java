package ru.tinkoff.jpa.demo.jpa.hibernate.row_mapper;


import lombok.RequiredArgsConstructor;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;
import ru.tinkoff.jpa.demo.jpa.hibernate.entities.Profile;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.UUID;

@Component
@RequiredArgsConstructor
public class ProfileRowMapper implements RowMapper<Profile> {

    @Override
    public Profile mapRow(ResultSet rs, int rowNum) throws SQLException {
        return Profile.builder()
            .id(rs.getObject("id", UUID.class))
            .position(rs.getLong("position"))
            .siebelId(rs.getString("siebel_id"))
            .nickname(rs.getString("nickname"))
            .build();
    }
}