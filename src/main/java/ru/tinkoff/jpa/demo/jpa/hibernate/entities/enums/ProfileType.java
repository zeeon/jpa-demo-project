package ru.tinkoff.jpa.demo.jpa.hibernate.entities.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum ProfileType {
    PERSONAL("personal"),
    CORPORATE("corporate");

    @Getter
    private final String value;
}
