package ru.tinkoff.jpa.demo.jpa.hibernate.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import ru.tinkoff.jpa.demo.jpa.hibernate.entities.Contract;
import ru.tinkoff.jpa.demo.jpa.hibernate.entities.Instrument;

import java.util.List;
import java.util.Optional;

@Repository
public interface InstrumentRepository extends JpaRepository<Instrument, Long> {

    @Query(nativeQuery = true, value = "SELECT * FROM instrument WHERE type = 'share' " +
        "  AND otc_flag = false ORDER BY random() LIMIT 1;")
    Optional<Instrument> findInstrumentByTypeAndOtcFlag();

    List<Instrument> findAllById(Long id);

    Optional<Instrument> findByTicker(String ticker);

    List<Instrument> findAllByTicker(String ticker);
}
