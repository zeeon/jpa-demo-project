package ru.tinkoff.jpa.demo.jpa.hibernate.entities.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum ProfileStatus {
    BLANK("blank"),
    OPEN("open"),
    CLOSE("close");

    @Getter
    private final String value;
}
