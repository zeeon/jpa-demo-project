package ru.tinkoff.jpa.demo.jpa.hibernate.services;

import io.qameta.allure.Step;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import ru.tinkoff.jpa.demo.jpa.hibernate.entities.Instrument;
import ru.tinkoff.jpa.demo.jpa.hibernate.repositories.InstrumentRepository;


import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.Optional;

import static ru.tinkoff.jpa.demo.helper.AllureHelper.*;
import static ru.tinkoff.jpa.demo.helper.AllureHelper.DELETED_ENTITIES;


@Slf4j
@Service
@RequiredArgsConstructor
public class InstrumentService {
    private final InstrumentRepository repository;

    @Step("Получить рандомный инструмент по type-share, otcFlag-true")
    public Instrument getRandomInstrument() {
        log.info("Получен запрос на получение instrument по type:{} otcFlag:{}", "share", "true");
        Optional<Instrument> instrument = repository.findInstrumentByTypeAndOtcFlag();
        if (!instrument.isPresent()) {
            throw new EntityNotFoundException("Инструмент не найден");
        }
        log.info("Найдены записи: {}", instrument);
        addJsonAttachment(FOUND_ENTITIES, instrument);
        return instrument.get();
    }

    @Step("Обновить instrument")
    public Instrument updateInstrument(Instrument instrument) {
        log.info("Получен запрос на обновление instrument данные: {}", instrument);
        repository.save(instrument);
        log.info("Обновлена запись");
        addJsonAttachment(ADDED_ENTITIES, instrument);
        return instrument;
    }

    @Step("Удалить instrument по id-{id}")
    public void deleteInstrument(Long id) {
        log.info("Получен запрос на удаление instrument по id: {}", id);
        List<Instrument> instruments = repository.findAllById(id);
        repository.deleteAll(instruments);
        log.info("Удалена запись {}", instruments);
        addJsonAttachment(DELETED_ENTITIES, instruments);
    }

    @Step("Получить список instrument по id-{id}")
    public List<Instrument> getListInstrument(Long id) {
        log.info("Получен запрос на получение list instrument по id: {}", id);
        List<Instrument> instruments = repository.findAllById(id);
        log.info("Найдена запись {}", instruments);
        addJsonAttachment(FOUND_ENTITIES, instruments);
        return instruments;
    }

    @Step("Получить инструмент по ticker-{ticker}")
    public Instrument getInstrumentByTicker(String ticker) {
        log.info("Получен запрос на получение instrument по ticker:{} ", ticker);
        Optional<Instrument> instrument = repository.findByTicker(ticker);
        if (!instrument.isPresent()) {
            throw new EntityNotFoundException("Инструмент не найден");
        }
        log.info("Найдены записи: {}", instrument);
        addJsonAttachment(FOUND_ENTITIES, instrument);
        return instrument.get();
    }

    @Step("Получить список инструмент по ticker-{ticker}")
    public List<Instrument> getListInstrumentByTicker(String ticker) {
        log.info("Получен запрос на получение list instrument по ticker:{} ", ticker);
        List<Instrument> instruments = repository.findAllByTicker(ticker);
        log.info("Найдены записи: {}", instruments);
        addJsonAttachment(FOUND_ENTITIES, instruments);
        return instruments;
    }
}
