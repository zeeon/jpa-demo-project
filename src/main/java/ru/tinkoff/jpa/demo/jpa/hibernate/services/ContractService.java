package ru.tinkoff.jpa.demo.jpa.hibernate.services;

import io.qameta.allure.Step;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import ru.tinkoff.jpa.demo.jpa.hibernate.entities.Contract;
import ru.tinkoff.jpa.demo.jpa.hibernate.entities.enums.ContractType;
import ru.tinkoff.jpa.demo.jpa.hibernate.repositories.ContractRepository;


import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static ru.tinkoff.jpa.demo.helper.AllureHelper.*;
import static ru.tinkoff.jpa.demo.helper.AllureHelper.DELETED_ENTITIES;


@Slf4j
@Service
@RequiredArgsConstructor
public class ContractService {
    private final ContractRepository repository;

    @Step("Получить контракт по profileId-{profileId}")
    public Contract getContractByProfileIdAndType(UUID profileId, ContractType type) {
        log.info("Получен запрос на получение contract по profileId: {} type: {}", profileId, type);
        Contract contract = repository.findFirstByProfileIdAndType(profileId, type);
        log.info("Найдены записи: {}", contract);
        addJsonAttachment(FOUND_ENTITIES, contract);
        return contract;
    }


    @Step("Получить contract по id-{}")
    public Contract getContractById(String id) {
        log.info("Получен запрос на получение contract по id: {}", id);
        Optional<Contract> contract = repository.findById(id);
        if (!contract.isPresent()) {
            throw new EntityNotFoundException("Контракт не найден");
        }
        log.info("Найдены записи: {}", contract);
        addJsonAttachment(FOUND_ENTITIES, contract);
        return contract.get();
    }

    @Step("Обновить contract")
    public Contract updateContract(Contract contract) {
        log.info("Получен запрос на обновление contract данные: {}", contract);
        repository.save(contract);
        log.info("Обновлена запись");
        addJsonAttachment(ADDED_ENTITIES, contract);
        return contract;
    }

    @Step("Удалить contract по id")
    public void deleteContract(String id) {
        log.info("Получен запрос на удаление contract по id: {}", id);
        List<Contract> contracts = repository.findAllById(id);
        repository.deleteAll(contracts);
        log.info("Удалена запись {}", contracts);
        addJsonAttachment(DELETED_ENTITIES, contracts);
    }
}
