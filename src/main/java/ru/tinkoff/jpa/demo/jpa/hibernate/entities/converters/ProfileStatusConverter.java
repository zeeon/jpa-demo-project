package ru.tinkoff.jpa.demo.jpa.hibernate.entities.converters;


import ru.tinkoff.jpa.demo.jpa.hibernate.entities.enums.ProfileStatus;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter
public class ProfileStatusConverter implements AttributeConverter<ProfileStatus, String> {
    @Override
    public String convertToDatabaseColumn(ProfileStatus attribute) {
        return attribute.getValue();
    }

    @Override
    public ProfileStatus convertToEntityAttribute(String dbData) {
        if (dbData == null) {
            throw new IllegalArgumentException();
        }
        for (ProfileStatus status : ProfileStatus.values()) {
            if (dbData.equals(status.getValue())) {
                return status;
            }
        }
        throw new IllegalArgumentException();
    }
}
